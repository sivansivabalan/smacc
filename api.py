from flask import Flask, jsonify
from flask import render_template
from flask import request
import sendgrid_email
import mailgun
app = Flask(__name__, static_url_path='/static/stylesheets')

@app.route("/")
def render_index():
    return render_template('index.html')

@app.route('/',methods=['GET', 'POST'])
def handle_data():
    try:
        input_form = request.form.getlist('name[]')
        print input_form
        print input_form.getlist('check_list')

        if input_form["email_address"] is None or input_form["subject_line"] is None or input_form["message"] is None or len(input_form.getlist("check_list")) == 0:
            return "Invalid Input please provide all the fileds"
        else:
            is_created = mailgun.get_all_input_fields_and_add_to_email(input_form["email_address"] , input_form["subject_line"] , input_form["message"], input_form.getlist('check_list'))
            is_created_sendgrid_email = sendgrid_email.get_all_input_fields_and_add_to_email(input_form["email_address"] , input_form["subject_line"] , input_form["message"], input_form.getlist('check_list'))

            if is_created:
                return "Your mail has been sent. Go back to main page <a href='/'> / </a>"
            else:
                return "Failed to send email, aplogies, please try by providing all valid input. <a href='/'> / </a>"
    except Exception as e:
        print e
        return "Fatal error, aplogies, please try again by providing all valid input. <a href='/'> / </a>"

if __name__ == "__main__":
    app.run(host="0.0.0.0",
        port=int("8080")
            )
