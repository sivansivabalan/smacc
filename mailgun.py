import requests
import api

def get_all_input_fields_and_add_to_email(email_address,subject_line,message):


	key = 'key-c8c7ff0bf9a8e0c1d41b1f310018dc56'
	sandbox = 'freaknerd.com'
	to_recipient = str(email_address)
	subject = str(subject_line)
	body = str(message)
	from_recipient = 'siva@freaknerd.com'

	request_url= "https://api.mailgun.net/v3/sandbox34a8c236548e44e8b57a0414f00359d5.mailgun.org/messages"
	request = requests.post(request_url, auth=('api', key), data={
    		'from': from_recipient,
    		'to': to_recipient,
    		'subject': subject,
    		'text': body
	})

	print 'Status: {0}'.format(request.status_code)
	print 'Body:   {0}'.format(request.text)

